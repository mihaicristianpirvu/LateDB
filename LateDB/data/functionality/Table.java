package data.functionality;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class Table implements Serializable {
	
	private static final long serialVersionUID = 569034109488731780L;
	private String name;
	private int numberOfLines;
	private ArrayList<Column> columns;
	private ArrayList<DataType> columnDataTypes;
	private ArrayList<ArrayList<Object>> lines;
	
	
	public Table(String name) {
		this.name = name;
		this.columns = new ArrayList<Column>();
		this.columnDataTypes = new ArrayList<DataType>();
		this.lines = new ArrayList<ArrayList<Object>>();
		
	}
	
	public static Object getCorrectValue(DataType dataType, String string) {
		Object value = null;
		
		if(dataType.equals(DataType.BOOLEAN))
			value = Boolean.parseBoolean(string);
		else if(dataType.equals(DataType.INTEGER))
			value = Integer.parseInt(string);
		else if(dataType.equals(DataType.DOUBLE))
			value = Double.parseDouble(string);
		else if(dataType.equals(DataType.STRING))
			value = string;

		return value;
	}
	
	public void addColumn(String name, DataType dataType) {
		
		try {
			Integer.parseInt(name);
			Double.parseDouble(name);
		}
		catch (NumberFormatException e){
			columns.add(new Column(name, dataType));
			columnDataTypes.add(dataType);
			return;
		}
		
		System.out.println("[Table] Tabele nu pot fi doar cifre/numere. ");
		
	}
	
	
	public void removeColumn(String name){
		int columnsSize = this.getColumns().size();
		
		for(int i=0; i<columnsSize; i++)
			if(this.getColumns().get(i).getName().equals(name)){
				this.getColumns().remove(i);
				this.getColumnsDataTypes().remove(i);
				break;
			}
	}
	
	public int getNumberOfLines(){
		return this.numberOfLines;
	}
	
	public Column getColumn(String name) {
		for (Column column : columns)
			if (column.getName().equals(name))
				return column;
		
		return null;
	}
	
	public int getColumnPosition(Column c) {
		int columnsSize = columns.size();
		for (int i=0; i<columnsSize; i++)
			if (columns.get(i).getName().equals(c.getName()))
				return i;
		
		return -1;
	}
	
	public void insert(ArrayList<Object> line) {
		
		this.lines.add(line);
			
		numberOfLines ++;
	}
	
	public void insert(List<? extends Object> line){
		ArrayList<Object> aLine = new ArrayList<Object>();
		aLine.addAll(line);
		this.insert(aLine);
	}

	public void insert(Object... objects){
		ArrayList<Object> line = new ArrayList<Object>();
		for(Object o : objects)
			line.add(o);
		this.insert(line);
	}
	
	public void removeLine(int number) {
		this.lines.remove(number);
		
		numberOfLines --;
	}
	
	public static <T> boolean contains(final T[] array, final T v) {
	    if (v == null) {
	        for (final T e : array)
	            if (e == null)
	                return true;
	    } else {
	        for (final T e : array)
	            if (e == v || v.equals(e))
	                return true;
	    }

	    return false;
	}
	
	// select("id, varsta, este_adult")
	public Table select(String columns) {
		String[] columnNames = columns.split(", ");
		Table temporary = new Table(this.name);
		ArrayList<Integer> columnIndices = new ArrayList<Integer>();
		
		int columnsSize = this.columns.size();
		for (int i = 0; i < columnsSize; i++) {
			Column column = this.columns.get(i);
			/* * sau numele coloanei se afla in coloanele cautate => adaug coloana la tabela noua si salvez indicele ei pt linii */
			if(columnNames[0].equals("*") || contains(columnNames, column.getName())) {
				temporary.addColumn(column.getName(), column.getDataType());
				columnIndices.add(i);
			}
		}
		
		/* Iau toate liniile la rand si adaug coloanele cerute */
		for(int line_i = 0; line_i < this.numberOfLines; line_i++){
			ArrayList<Object> newLine = new ArrayList<Object>();
			for(Integer i : columnIndices)
				newLine.add(this.getLines().get(line_i).get(i));
			temporary.insert(newLine);
		}
		
		return temporary;
	}
	
	
	/* x = 5, y = 4 WHERE x>5 AND id<100*/
	public Table update(String updates, String condition) {
		Table changeTable = this.where(condition);
		Table diffTable = this.difference(changeTable);
		String[] changes = updates.split(", ");
		ArrayList<Integer> changeIndex = new ArrayList<Integer>();
		/* Sa nu fac split de 2 ori.. */
		ArrayList<Object> parsedValues = new ArrayList<Object>();
		
		/* Iau fiecare schimbare si vad pe ce coloana trebuie pusa (pentru fiecare linie) */
		for(int i=0; i<changes.length; i++){
				
			for( int j=0; j<changeTable.getColumns().size(); j++){
				Column c = changeTable.getColumns().get(j);
				String[] changeName = changes[i].split("=");
				String[] splitName = changeName[0].split("\\.");
				if(c.getName().equals(changeName[0]) || ( splitName.length > 1 && (c.getName().equals(splitName[1])) ) ){
					changeIndex.add(j);
					parsedValues.add(Table.getCorrectValue(c.getDataType(), changeName[1]));
					break;
				}
			}
		}
		
		/* Iau fiecare linie din cele ce trebuiesc schimbate*/
		for(int line_i = 0; line_i < changeTable.numberOfLines; line_i++){
			/* Aplic fiecare schimbare */
			for(int i = 0; i<changeIndex.size(); i++){
				changeTable.getLines().get(line_i).set(changeIndex.get(i), parsedValues.get(i));
			}
		}
		
		return diffTable.union(changeTable);
	}
	
	
	public Table delete(String condition) {
		Table table = this.where(condition);
		
		return this.difference(table);
	}
	
	
	public Table truncate() {
		Table table = new Table(name);
		
		//se adauga doar coloanele fara datele din ele
		for (Column column : this.columns)
			table.addColumn(column.getName(), column.getDataType());
		
		return table;
	}

	
	// Pentru AND
	// Trebuie sa se asigure ca cele doua tabele (this si table au acelasi antet, adica aceleasi coloane)
	// Pentru cazul de join, sigur se elimina niste coloane, deci trebuie sa gasesc antetul comun dintre cele doua
	public Table intersect(Table table){
		Table result = this.joinColumns(table).createTruncatedTable();
		
		// creez liniile cu antetul comun
		ArrayList<ArrayList<Object>> thisLines = this.joinColumns(table).getLines();
		ArrayList<ArrayList<Object>> otherLines = table.joinColumns(this).getLines();
		
		for(ArrayList<Object> line : thisLines)
			for(ArrayList<Object> otherLine : otherLines)
				if(line.equals(otherLine)) {
					result.insert(line);
					break;
				}
				
		return result;
	}
	
	// Pentru OR
	public Table union(Table table){
		Table result = this.joinColumns(table).createTruncatedTable();
		ArrayList<ArrayList<Object>> thisLines = this.joinColumns(table).getLines();
		
		for(ArrayList<Object> line : thisLines)
			result.insert(line);
		
		if(table == null)
			return result;
		
		ArrayList<ArrayList<Object>> otherLines = table.joinColumns(this).getLines();
		
		for(ArrayList<Object> otherLine : otherLines){
			boolean found = false;
			
			for(ArrayList<Object> line : thisLines)
				if(otherLine.equals(line)){
					found = true;
					break;
				}
			
			// nu adaug duplicate
			if(!found)
				result.insert(otherLine);
		}
		
		return result;
	}
	
	public Table difference(Table table) {
		Table result = this.createTruncatedTable();
		ArrayList<ArrayList<Object>> thisLines = this.getLines();
		ArrayList<ArrayList<Object>> otherLines = table.getLines();
		
		for (ArrayList<Object> line : thisLines)
			if (!otherLines.contains(line))
				result.insert(line);
		
		return result;
	}
	
	/* Functie care returneaza true daca (dataType)object condition (dataType)value returneaza true
	 * Altfel, false
	 * */
	
	private boolean dataCompare(Object object, Object value, int conditionType, DataType dataType) {
		if(conditionType < 0 || conditionType > 5)
			return false;

		
		if(dataType == DataType.INTEGER){
			Integer iObject = (Integer)object;
			Integer iValue = (Integer)value;
			
			switch (conditionType){
				case 0: return !iObject.equals(iValue);
				case 1: return iObject >= iValue;
				case 2: return iObject > iValue;
				case 3: return iObject <= iValue;
				case 4: return iObject < iValue;
				case 5: return iObject.equals(iValue);
				default: return false;
			}
		}
		else if(dataType == DataType.DOUBLE){
			Double dObject = (Double)object;
			Double dValue = (Double)value;
			
			
			switch (conditionType){
				case 0: return !dObject.equals(dValue);
				case 1: return dObject >= dValue;
				case 2: return dObject > dValue;
				case 3: return dObject <= dValue;
				case 4: return dObject < dValue;
				case 5: return dObject.equals(dValue);
				default: return false;
			}
		}
		else if(dataType == DataType.BOOLEAN){
			Boolean bObject = (Boolean)object;
			Boolean bValue = (Boolean)value;
			
			switch (conditionType){
				case 0: return !bObject.equals(bValue);
				case 5: return bObject.equals(bValue);
				default: return false;
			}
		}
		else if(dataType == DataType.STRING){
			String sObject = (String)object;
			String sValue = (String)value;
			
			switch (conditionType){
				case 0: return !sObject.equals(sValue);
				case 5: return sObject.equals(sValue);
				default: return false;
			}
		}
		
		return false;
	}

	
	// WHERE a = 5, a > 5, a < 5, a != 5
	public Table condition(String condition){
		
		if(condition == "true" || condition == "TRUE")
			return this.deepCloneTable();
		
		Table table = this.createTruncatedTable();

		condition = condition.replaceAll("\\s", "");
		int conditionType = -1;
		String[] token;
		if( (token = condition.split("!==|!=")).length > 1)
			conditionType = 0;
		else if( (token = condition.split(">=")).length > 1)
			conditionType = 1;
		else if( (token = condition.split(">")).length > 1)
			conditionType = 2;
		else if( (token = condition.split("<=")).length > 1)
			conditionType = 3;
		else if( (token = condition.split("<")).length > 1)
			conditionType = 4;
		else if( (token = condition.split("==|=")).length > 1)
			conditionType = 5;

		Column c = this.getColumn(token[0]);
		
		if(c == null){
			
			String[] colSplit = token[0].split("\\.");
			if(colSplit.length > 1)
				c = this.getColumn(colSplit[1]);
			if(c == null) {
				System.out.println("[" + table.getName() + "] " + "Coloana " + token[0] + " nu exista");
				return table;
			}
		}
		
		Object object, value = null;
		DataType dataType = c.getDataType();
		int columnPosition = table.getColumnPosition(c);
		int otherColumnPosition = -1;

		Column c2 = table.getColumn(token[1]);
		/* c2 nu este tot o coloana => condition simpla */
		if(c2 == null){
			value = Table.getCorrectValue(dataType, token[1]);

			for(int i=0; i<this.numberOfLines; i++){
				object = this.lines.get(i).get(columnPosition);
				if(dataCompare(object, value, conditionType, dataType))
					table.insert(this.deepCloneLine(this.getLine(i)));
			}
			
			return table;
		}
		/* c2 este tot o coloana, verific daca are aceeasi baza (e conditie de join sau nu) */
		else {
			otherColumnPosition = table.getColumnPosition(c2);
			
			/* Ambele contin ., dar trebuie sa verific bazele */
			if(token[1].contains(".") && token[0].contains(".")){
				String[] part1 = token[1].split("\\.");
				String[] part2 = token[0].split("\\.");
				
				/* Conditie de join => tabela noua nu va avea cea de-a doua coloana ca si rezultat */
				if(!part1[0].equals(part2[0])){
					int position = -1;
					for(position = 0; position < table.getColumns().size(); position++)
						if(table.getColumns().get(position).getName().equals(token[1])){
							table.getColumns().remove(position);
							table.getColumnsDataTypes().remove(position);
							break;
						}
													
					for(int i=0; i<this.numberOfLines; i++){
						/* Sterg coloana position, pentru ca am facut join intre ea si coloana part1 */
						object = this.lines.get(i).get(columnPosition);
						value = this.lines.get(i).get(otherColumnPosition);
						if(dataCompare(object, value, conditionType, dataType)){
							ArrayList<Object> line = this.deepCloneLine(this.getLine(i));
							line.remove(position);
							table.insert(line);
						}
					}
					
					return table;
				}
			}
			
			/* Daca am ajuns aici, atunci c2 este o coloana simpla, care nu va fi eliminata din rezultat */
			for(int i=0; i<this.numberOfLines; i++){
				object = this.lines.get(i).get(columnPosition);
				value = this.lines.get(i).get(otherColumnPosition);
				if(dataCompare(object, value, conditionType, dataType))
					table.insert(this.deepCloneLine(this.getLine(i)));
			}
			
			return table;

		}
	}

	public Table where(String condition){
		/* Tipurile de conditii vor fi de 3 feluri: 
		 * - cond1 AND cond2
		 * - cond1 OR  cond2
		 * - (cond1 X cond2) X (cond3 X cond4)
		 * - AND > OR ca si prioritate, deci pentru simplitate voi trata ce-i in stanga si-n dreapta la OR ca o paranteza
		 * adica cond1 OR cond2 AND cond3 => cond1 OR (cond2 AND cond3)
		 * si    cond1 AND cond2 OR cond3 => (cond1 AND cond2) OR cond3
		 * Voi parsa fiecare paranteza gasita si o voi trimite acestei functii pana cand voi ajunge la conditii atomice
		 * pe care le trimit functiei condition
		 */
		
		int len = condition.length();
		if(len == 0)
			return this.createTruncatedTable();
		else if(condition.equals("true"))
			return this;
		
		/* Caut intai primul OR de nivel 0 (fara parenteze) si aplic union (where(left), where(right))
		 * Daca nu exista, caut primul AND de nivel 0 si aplic intersect(where(left), where(right))
		 * Altfel, inseamna ca sunt intr-o paranteza, deci o elimin, deci where(condition.substr(1, len-1))
		 * In final, am scazut un nivel, deci aplic acelasi lucru
		 * Cand ajung la o operatie simpla, intorc rezultatul functiei condition
		 */
		
		int level = 0, i = 0;
		// salvam prima pozitie a celui mai prioritar caz (util pentru cand exista doar AND, nu si OR)
		int firstLevelZero = -1; 
		boolean checkLevelZero = false; //verificam daca am intrat macar odata sa verificam level 0 (nu suntem intr-o paranteza)
		if(condition.charAt(0) == '('){
			level = 1;
			i = 1;
		}
		
		
		for(; i<len; i++){
			// trecem peste nivelurile superioare fara sa ne intereseze la iteratia aceasta
			if(condition.charAt(i) == '('){
				level++;
				continue;
			}
			else if(condition.charAt(i) == ')'){
				level--;
				continue;
			}
						
			if(level > 0){
				continue;
			}
			
			checkLevelZero = true;
					
			// altfel suntem pe nivel 0, cautam deci primul OR sau AND
			if(i+5 < len && (condition.substring(i, i+5).equals(" AND ") || condition.substring(i, i+5).equals(" and ")) && firstLevelZero == -1){
				firstLevelZero = i;
				i+=4;
				continue;
			}
			
			// am gasit un OR de nivel 0 => intram in recursivitate, caci e cel mai prioritar pe care o sa-l gasim
			if(i+4 < len && (condition.substring(i, i+4).equals(" OR ") || condition.substring(i, i+4).equals(" or ")) ){
				return this.where(condition.substring(0, i)).union(this.where(condition.substring(i+4, len)));
			}
				
		}
		
		// elimin paranteza
		if(checkLevelZero == false)
			return this.where(condition.substring(1, len-1));
		// altfel, stiu ca nu am gasit niciun OR, verific daca am gasit un AND
		else if(firstLevelZero != -1)
			return this.where(condition.substring(0, firstLevelZero)).intersect(this.where(condition.substring(firstLevelZero+5, len)));
		// altfel este o operatie atomica (x>5, de exemplu), deci intorc tabela pe o conditie atomica
		else
			return this.condition(condition);
		
	}
	
	
	public void print() {
		for (int i = 0; i < columns.size(); i ++)
			System.out.print(columns.get(i).getName() + "\t\t");
		
		for (int i = 0; i < numberOfLines; i ++) {
			System.out.println();
			
			for (int j = 0; j < columns.size(); j ++)
				System.out.print(this.lines.get(i).get(j) + "\t\t");
		}
		
		System.out.println("\n");
	}
	
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	
	public int getLineCount(){
		return this.numberOfLines;
	}
	
	public void setLineCount(int count){
		this.numberOfLines = count;
	}
	
	
	public ArrayList<Column> getColumns(){
		return this.columns;
	}
	
	
	public ArrayList<DataType> getColumnsDataTypes(){
		return this.columnDataTypes;
	}
	
	
	public ArrayList<Object> getLine(int index){
		return this.lines.get(index);
	}
	
	
	public ArrayList<ArrayList<Object>> getLines(){
		return this.lines;
	}

	
	public Table createTruncatedTable() {
		Table table = new Table(name);
		for(Column c : this.getColumns())
			table.addColumn(c.getName(), c.getDataType());
	
		
		return table;
	}
	
	
	public boolean equals(Table table){	
		if(table == null)
			return false;
		
		ArrayList<ArrayList<Object>> thisLines = this.getLines();
		ArrayList<ArrayList<Object>> otherLines = table.getLines();
		
		for(ArrayList<Object> line : thisLines){
			if(otherLines.contains(line) == false)
				return false;
		}
		
		return true;
	}
	
	public Object deepCloneValue(Object o, DataType dataType){
		
		if(dataType.equals(DataType.BOOLEAN))
			return new Boolean( (boolean)o );
		else if(dataType.equals(DataType.INTEGER))
			return new Integer( (int)o );
		else if(dataType.equals(DataType.DOUBLE))
			return new Double( (double)o );
		else if(dataType.equals(DataType.STRING))
			return new String( (String)o );
		
		return null;
		
	}
	
	public ArrayList<Object> deepCloneLine(ArrayList<Object> line){
		ArrayList<Object> newLine = new ArrayList<Object>();
		int lineSize = line.size();
		
		/* Stim maparea Coloane -> Linii, deci putem afla tipul de date pe baza indicelui */
		for(int i=0; i<lineSize; i++){
			newLine.add(this.deepCloneValue(line.get(i), this.columnDataTypes.get(i)));		
		}
		
		return newLine;
	}
	
	public Table deepCloneTable(){
		Table t = new Table(name);
					
		/* Recreez antetul tabelei curente, dar pun nume.numeColoana (de modificat pe viitor poate)*/
		for(int i=0; i<this.getColumns().size(); i++)
			t.addColumn( this.getName() + "." + this.getColumns().get(i).getName(), this.getColumnsDataTypes().get(i) );
		
		/* Iau fiecare linie, si creez una noua pe baza ei*/
		for(int i=0; i<this.getLineCount(); i++)
			t.insert(this.deepCloneLine(this.getLines().get(i)));
		
		
		return t;
	}
	
	
	/* Pe baza tabelei primite, creez o tabela noua care contine doar coloanele comune (nu o modific pe cea primita) */
	public Table joinColumns(Table table){
		if(table == null)
			return null;
		
		Table newTable = this.createTruncatedTable();
		ArrayList<Integer> removedIndexes = new ArrayList<Integer>();
		
		for(int i=0; i<newTable.getColumns().size(); i++){
			Column c = newTable.getColumns().get(i);
			boolean found = false;
			for(Column tc : table.getColumns())
				if(tc.getName().equals(c.getName())){
					found = true;
					break;
				}
			if(found == false){
				removedIndexes.add(i);
				newTable.getColumns().remove(i); //sterg tabela care nu exista dincolo
				newTable.getColumnsDataTypes().remove(i); //si tipul ei
			}
		}
		
		// sterg elementele necorespunzatoare din liniile tabelei mele pentru tabela noua
		for(ArrayList<Object> line : lines){
			
			ArrayList<Object> newLine = this.deepCloneLine(line);
			for(Integer i : removedIndexes)
				newLine.remove(i.intValue());
		
			newTable.insert(newLine);
		}
	
		return newTable;
	}
	
	public void purge(){
		this.lines = new ArrayList<ArrayList<Object>>();
		this.numberOfLines = 0;
	}
	
}
