package data.user_interface;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import data.functionality.Database;
import data.functionality.Table;
import data.user_interface.panels.MainPanel;


@SuppressWarnings("serial")
public class LateDB extends JFrame {
	
	private ArrayList<Database> databases;
	private JPanel mainPanel;
	
	private Object[] options = {"Da", "Nu"};
	
	
	public LateDB() {
		super("LateDB");
		showSplashScreen();
		setLookAndFeel();
		setWindow();
		loadDatabases();
		
		mainPanel = new MainPanel(this);
		
		getContentPane().add(mainPanel);
		setVisible(true);
	}
	
	
	private void showSplashScreen() {
		SplashScreen splashScreen = new SplashScreen();
    	
    	try {
			Thread.sleep(3000);
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		}
    	
    	splashScreen.stop();
	}
	
	
	//functie ce seteaza design-ul interfetei grafice
	private void setLookAndFeel() {
		try {
	        for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
	            if ("Nimbus".equals(info.getName())) {
	                UIManager.setLookAndFeel(info.getClassName());
	                break;
	            }
	        }
	    }     
	    catch (Exception e) {
	    	try {
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			}
	    	catch (Exception e1) {
	    		e1.printStackTrace();
	    	}
	    }
	}
	
	
	//functie ce construieste fereastra aplicatiei
	private void setWindow() {
		Toolkit t = getToolkit();
	  	Image img = t.getImage("data/images/logo.png");
	  	setIconImage(img);
	  	
	  	setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	  	setSize(800, 600);
	  	setResizable(false);
	  	setLocationRelativeTo(null);
	  	addWindowListener(new WindowListener() {
			public void windowActivated(WindowEvent arg0) {	}
			public void windowClosed(WindowEvent arg0) {  }
			public void windowClosing(WindowEvent arg0) {
				//setare mesaj de confirmare la apasarea butonului X al ferestrei
				int YES_OPTION = 0, click = JOptionPane.showOptionDialog(LateDB.this,
					"Sunteţi sigur(ă) că doriți să închideţi programul?", "LateDB", 
					JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, 
					null, options, options[0]);
				
				if (click == YES_OPTION) {
					saveDatbases();
					System.exit(0);
				}
			}
			public void windowDeactivated(WindowEvent arg0) { }
			public void windowDeiconified(WindowEvent arg0) { }
			public void windowIconified(WindowEvent arg0) {	}
			public void windowOpened(WindowEvent arg0) { } 
		});
	}
	
	
	private void saveDatbases() {
		try {
			FileOutputStream fout = new FileOutputStream("data/databases.dat");
			ObjectOutputStream oos = new ObjectOutputStream(fout);
			
			//scriem array list-ul de baze de date in fisier
			oos.writeObject(databases);
			
			oos.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	@SuppressWarnings("unchecked")
	private void loadDatabases() {
		try {
			FileInputStream fin = new FileInputStream("data/databases.dat");
			ObjectInputStream ois = new ObjectInputStream(fin);
			
			//citim array list-ul de baze de date din fisier
			databases = (ArrayList<Database>)ois.readObject();
			
			ois.close();
		}
		catch (IOException | ClassNotFoundException e) {
			//a aparut o eroare, deci vom crea o noua lista de baze de date
			databases = new ArrayList<Database>();
		}
	}
	
	
	public boolean addDatabase(String name) {
		//verificam sa nu adaugam o baza de date cu acelasi nume
		for (int i = 0; i < databases.size(); i ++)
			if (databases.get(i).getName().equals(name))
				return false;
		
		databases.add(new Database(name));
		
		return true;
	}
	
	public void removeDatabase(String name) {
		for (int i = 0; i < databases.size(); i ++)
			if (databases.get(i).getName().equals(name)) {
				databases.remove(i);
				break;
			}
	}
	
	public ArrayList<Database> getDatabases() {
		return databases;
	}
	
	
	public ArrayList<Table> getTables(String databaseName) {
		for (int i = 0; i < databases.size(); i ++)
			if (databases.get(i).getName().equals(databaseName))
				return databases.get(i).getTables();
		
		return null;
	}
	
}
