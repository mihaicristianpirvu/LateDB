package data.user_interface;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JWindow;


public class SplashScreen {
	
	private JWindow splashScreen;
	
	
	public SplashScreen() {
		splashScreen = new JWindow();
		setWindow();
	}
	
	
	private void setWindow() {
		JPanel panel = new JPanel();
		
		JLabel splash = new JLabel(new ImageIcon("data/images/splash.png"));
		panel.add(splash);
		
		splashScreen.getContentPane().add(panel);
		splashScreen.pack();
		splashScreen.setLocationRelativeTo(null);
		splashScreen.setVisible(true);
	}
	
	
	public void stop() {
		splashScreen.dispose();
	}
	
}
