package data.user_interface;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;


@SuppressWarnings("serial")
public class JLabelCellRenderer extends JLabel implements ListCellRenderer<Object> {

	public JLabelCellRenderer() {
		setOpaque(true);
	}
	
	
	//metoda ce descrie modul cum vor fi afisate elementele JLabel in lista de pe interfata grafica
	public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
		JLabel label = JLabel.class.cast(value);
		
		setText(label.getText());
		setIcon(label.getIcon());
		
		if (isSelected)
			setBackground(Color.LIGHT_GRAY);
		else
			setBackground(Color.WHITE);
		
		setForeground(label.getForeground());
		
		return this;
	}
	
}
