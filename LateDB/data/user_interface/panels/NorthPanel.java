package data.user_interface.panels;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import data.user_interface.LateDB;


@SuppressWarnings("serial")
public class NorthPanel extends JPanel {
	
	private LateDB window;
	private MainPanel mainPanel;
	
	private Object[] options = {"Da", "Nu"};
	
	
	public NorthPanel(LateDB window, MainPanel mainPanel) {
		super();
		this.window = window;
		this.mainPanel = mainPanel;
		
		setPanel();
	}
	
	
	private void setPanel() {
		JButton addButton = new JButton("<html><b>Adaugă bază de date</b></html>", new ImageIcon("data/images/addDatabase.png"));
		addButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String name = (String)JOptionPane.showInputDialog(window, "Introduceți numele bazei de date:", "LateDB",
																  JOptionPane.PLAIN_MESSAGE, null, null, null);
	        	
				if (name != null && !name.equals("")) {
					//functia addDatabase intoarce false cand avem deja o baza de date in sistem cu numele trimis ca parametru
					boolean ok = window.addDatabase(name);
					
					if (ok == true) {
						//adaugam baza de date si pe lista din interfata grafica
						mainPanel.getListModel().addElement(new JLabel(name, new ImageIcon("data/images/database.png"), JLabel.CENTER));
						window.invalidate();
						window.revalidate();
						window.repaint();
						
						JOptionPane.showMessageDialog(window, "Baza de date s-a adăugat cu succes!", "LateDB", JOptionPane.INFORMATION_MESSAGE);
					}
					else
						JOptionPane.showMessageDialog(window, "Există deja o bază de date cu același nume!", "LateDB", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		add(addButton);
		
		JButton deleteButton = new JButton("<html><b>Șterge baza de date</b></html>", new ImageIcon("data/images/delete.png"));
		deleteButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int YES_OPTION = 0, click = JOptionPane.showOptionDialog(window,
						"Sunteţi sigur(ă) că doriți să ștergeți baza de date?", "LateDB", 
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, 
						null, options, options[0]);
				
				if (click == YES_OPTION) {
					String selectedDatabaseName = mainPanel.getSelectedDatabase();
					DefaultListModel<JLabel> listModel = mainPanel.getListModel();
					
					//scoatem baza de date din sistem
					window.removeDatabase(selectedDatabaseName);
					
					//si din lista afisata pe intefata grafica
					for (int i = 0; i < listModel.getSize(); i ++)
						if (listModel.getElementAt(i).getText().equals(selectedDatabaseName)) {
							listModel.removeElementAt(i);
							break;
						}
					
					mainPanel.resetCenterPanel();
				}
			}
		});
		deleteButton.setEnabled(false);
		mainPanel.setDeleteButton(deleteButton);
		add(deleteButton);
	}

}
