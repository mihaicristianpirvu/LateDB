package data.user_interface.panels;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import data.functionality.Database;
import data.user_interface.JLabelCellRenderer;
import data.user_interface.LateDB;


@SuppressWarnings("serial")
public class WestPanel extends JPanel {
	
	private LateDB window;
	private MainPanel mainPanel;
	
	private JList<JLabel> listOfDatabases;
	private JPopupMenu rightClick = new JPopupMenu("Opțiuni");
	private Object[] options = {"Da", "Nu"};
	
	
	public WestPanel(LateDB window, MainPanel mainPanel) {
		super(new BorderLayout());
		this.window = window;
		this.mainPanel = mainPanel;
		
		setPanel();
	}
	
	
	private void setPanel() {
		setBorder(new EmptyBorder(0, 10, 10, 0));
		
		JLabel text = new JLabel("<html><b>Baze de date existente:</b></html>");
		add(text, BorderLayout.NORTH);
		
		setListOfDatabases();
		
		//setam sa avem scroll bar la lista de baze de date
		JScrollPane listScroller = new JScrollPane(listOfDatabases);
		listScroller.setPreferredSize(new Dimension(100, 100));
		
		add(listScroller, BorderLayout.CENTER);
		
		//setare meniu click dreapta
		JMenuItem rename = new JMenuItem("Redenumire", new ImageIcon("data/images/renameMini.png"));
		rename.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String name = (String)JOptionPane.showInputDialog(window, "Introduceți noul nume al bazei de date:", "LateDB",
						  JOptionPane.PLAIN_MESSAGE, null, null, null);

				if (name != null && !name.equals("")) {
					ArrayList<Database> databases = window.getDatabases();
					String selectedDatabase = mainPanel.getSelectedDatabase();
					int position = -1;
					
					//verificam sa nu punem un nume duplicat
					for (int i = 0; i < databases.size(); i ++)
						//daca una din bazele de date neselectate are numele egal cu ce am dat ca parametru vom marca acest lucru si vom iesi din ciclu
						if (!databases.get(i).getName().equals(selectedDatabase) && databases.get(i).getName().equals(name)) {
							position = -1;
							break;
						}
						else if (databases.get(i).getName().equals(selectedDatabase))
							position = i;
					
					if (position != -1) {
						databases.get(position).setName(name);
						listOfDatabases.getSelectedValue().setText(name);
						
						JOptionPane.showMessageDialog(window, "Numele bazei de date a fost schimbat cu succes!", "LateDB", JOptionPane.INFORMATION_MESSAGE);
					}
					else
						JOptionPane.showMessageDialog(window, "Există deja o bază de date cu același nume!", "LateDB", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		rightClick.add(rename);
		
		JMenuItem delete = new JMenuItem("Ștergere", new ImageIcon("data/images/deleteMini.png"));
		delete.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int YES_OPTION = 0, click = JOptionPane.showOptionDialog(window,
						"Sunteţi sigur(ă) că doriți să ștergeți baza de date?", "LateDB", 
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, 
						null, options, options[0]);
				
				if (click == YES_OPTION) {
					String selectedDatabaseName = mainPanel.getSelectedDatabase();
					DefaultListModel<JLabel> listModel = mainPanel.getListModel();
					
					//scoatem baza de date din sistem
					window.removeDatabase(selectedDatabaseName);
					
					//si din lista afisata pe intefata grafica
					for (int i = 0; i < listModel.getSize(); i ++)
						if (listModel.getElementAt(i).getText().equals(selectedDatabaseName)) {
							listModel.removeElementAt(i);
							break;
						}
					
					mainPanel.resetCenterPanel();
				}
			}
		});
		rightClick.add(delete);
	}
	
	
	private void setListOfDatabases() {
		DefaultListModel<JLabel> listModel = new DefaultListModel<JLabel>();
		mainPanel.setListModel(listModel);
		listOfDatabases = new JList<JLabel>(listModel);
		listOfDatabases.setCellRenderer(new JLabelCellRenderer());
		listOfDatabases.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		listOfDatabases.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.getClickCount() == 1 && e.getButton() == 3 && mainPanel.getSelectedDatabase() != null) {
					rightClick.show(listOfDatabases, e.getX(), e.getY());
					rightClick.setVisible(true);
				}
			}
		});
		
		listOfDatabases.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				//setam numele bazei de date selectate
				if (listOfDatabases.getSelectedValue() == null) {
					mainPanel.setSelectedDatabase(null);
					mainPanel.getDeleteButton().setEnabled(false);
				}
				else {
					mainPanel.setSelectedDatabase(listOfDatabases.getSelectedValue().getText());
					mainPanel.getDeleteButton().setEnabled(true);
					mainPanel.populateCenterPanel(listOfDatabases.getSelectedValue().getText());
				}
			}
		});
		
		//adaugam tabelele ce exista in fisierul incarcat la pornirea programului
		for (Database database : window.getDatabases())
			mainPanel.getListModel().addElement(new JLabel(database.getName(), new ImageIcon("data/images/database.png"), JLabel.CENTER));
	}

}
