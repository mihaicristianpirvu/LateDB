package data.tests;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import data.functionality.*;

public class Tests {
	
	public static boolean critical = false;
	public static Database db;
	public static int numberOfTests = 0;
	public static int successTests = 0;
	
	
	public static void RunTests(){
		critical = true;
			
		/* Sanity checks */
		TestCreate();
		TestCreateTables();
		TestCreateColumns();
		TestAddLines();
		
		/* Cartezian product checks */
		TestBasicJoin();
		TestCartezianProduct();
		
		/* Condition, Update, Delete, Union, Intersect and Where checks */
		TestSimpleCondition();
		TestUpdate();
		TestDelete();
		TestUnion();
		TestIntersect();
		TestComplexCondition();
		
		/* Test for joins between tables */
		TestSimpleJoin();
		TestComplexJoin();
		
		System.out.println("[TEST] All test finished. Rate of succes: " + successTests + " of " + numberOfTests + " [" + (double)successTests/numberOfTests*100 + "%]");
	}

	public static boolean basic_test(boolean condition){
		return basic_test(condition, "[TEST] Error");
	}
	
	public static boolean basic_test(boolean condition, String message){
		numberOfTests++;
		
		if(condition == false){
			System.out.println(message);
			return false;
		}
		
		successTests++;
		return true;
	}
	
	public static void TestCreate(){
		String dbName = "dbTest";
		db = new Database(dbName);

		basic_test(db instanceof Database && db.getName().equals(dbName),
					"[Test] Eroare la crearea bazei de date");
	}
	
	public static void TestCreateTables(){
		TestCreate();
		String tableName = "persoane";
		
		db.addTable(tableName);
		Table table = db.getTable(tableName);
		basic_test(table != null && table instanceof Table && table.getName().equals(tableName),
					"[Test] Eroare la crearea unei tabele");
	
	}
	
	public static void TestCreateColumns(){
		TestCreateTables();
		Table table = db.getTable("persoane");
		
		table.addColumn("id", DataType.INTEGER);
		table.addColumn("nume", DataType.STRING);
		table.addColumn("varsta", DataType.INTEGER);
		table.addColumn("este_adult", DataType.BOOLEAN);
		
		basic_test(table.getColumn("id") != null && table.getColumn("nume") != null &&
					table.getColumn("varsta") != null && table.getColumn("este_adult") != null &&
					table.getColumn("asdf") == null, "[Test] Eroare la adaugarea coloanelor la o tabela");
	}
	
	public static void TestAddLines(){
		TestCreateColumns();
		Table table = db.getTable("persoane");
		
		table.insert(1, "Vali", 22, true);
		table.insert(2, "Mihai", 17, false);
		table.insert(3, "Marian", 22, true);
		
		basic_test(table.getLineCount() == 3, "[Test] Eroare la adaugarea liniilor");
	}
	
	public static void TestBasicJoin(){
		TestCreate();
		String test1 = "test1", test2 = "test2";
		db.addTable(test1);
		db.addTable(test2);
		
		Table table1 = db.getTable(test1);
		Table table2 = db.getTable(test2);
		
		table1.addColumn("col1", DataType.INTEGER);
		table1.addColumn("col2", DataType.INTEGER);
		table1.addColumn("col3", DataType.INTEGER);

		table2.addColumn("col1", DataType.INTEGER);
		table2.addColumn("col2", DataType.INTEGER);
		table2.addColumn("col3", DataType.INTEGER);
		
		Table joinTable = db.join(table1.getName() + ", " + table2.getName());
		
		basic_test(joinTable.getColumns().size() == table1.getColumns().size() + table2.getColumns().size());
	}
	
	public static void TestCartezianProduct(){
		TestCreate();
		String test1 = "test1", test2 = "test2";
		db.addTable(test1);
		db.addTable(test2);
		
		Table table1 = db.getTable(test1);
		Table table2 = db.getTable(test2);
		
		table1.addColumn("col1", DataType.INTEGER);
		table1.addColumn("col2", DataType.INTEGER);
		table1.addColumn("col3", DataType.BOOLEAN);

		table1.insert(1, 1, true);		
		table1.insert(2, 2, true);
		table1.insert(3, 1, false);	
		
		table2.addColumn("col1", DataType.INTEGER);
		table2.addColumn("col2", DataType.STRING);
		table2.addColumn("col3", DataType.BOOLEAN);
		
		table2.insert(1, "gigi", true);	
		table2.insert(2, "dan", true);
		table2.insert(3, "mimi", false);
		
		Table joinTable = db.join(table1.getName() + ", " + table2.getName());
		
		basic_test(joinTable.getLineCount() == table1.getLineCount() * table2.getLineCount());
			
	}
	
	static void TestSimpleCondition(){
		db = new Database("yey");
		
		ArrayList<Object> line = new ArrayList<Object>();
		
		db.addTable("testTable1");
		Table table1 = db.getTable("testTable1");
		table1.addColumn("col1", DataType.INTEGER);
		table1.addColumn("col2", DataType.STRING);
		table1.addColumn("col3", DataType.BOOLEAN);
		table1.addColumn("col4", DataType.DOUBLE);
				
		line.addAll(Arrays.asList(1, "test", true, 3.5));
		table1.insert(line);
		line = new ArrayList<Object>();
		
		line.addAll(Arrays.asList(2, "test1q", true, 4.6));
		table1.insert(line);
		line = new ArrayList<Object>();
		
		line.addAll(Arrays.asList(3, "atest", true, 99.99));
		table1.insert(line);
		
		Table _table1 = table1.condition("col4=99.99");
		Table _table2 = table1.condition("col4>3.5");
		Table _table3 = table1.condition("col2=test1q");
		Table _table4 = table1.condition("col4!=4.6");

		
		basic_test(_table1.getLineCount() == 1 && _table2.getLineCount() == 2 && 
				   _table3.getLineCount() == 1 && _table4.getLineCount() == 2);
	}
	
	static void TestUpdate() {
		db = new Database("yey");
		
		ArrayList<Object> line = new ArrayList<Object>();
		
		db.addTable("testTable1");
		Table table1 = db.getTable("testTable1");
		table1.addColumn("col1", DataType.INTEGER);
		table1.addColumn("col2", DataType.STRING);
		table1.addColumn("col3", DataType.BOOLEAN);
		table1.addColumn("col4", DataType.DOUBLE);
		
		line.addAll(Arrays.asList(1, "test", true, 3.5));
		table1.insert(line);
		line = new ArrayList<Object>();

		line.addAll(Arrays.asList(2, "test1q", true, 4.6));
		table1.insert(line);
		line = new ArrayList<Object>();
		
		line.addAll(Arrays.asList(3, "atest", true, 99.99));
		table1.insert(line);
		
		Table _table1 = table1.update("testTable1.col4=4.4", "testTable1.col2=test");
		
		basic_test(_table1.condition("col4=3.5").getLineCount() == 0);
	}
	
	static void TestDelete() {
		db = new Database("test");
		ArrayList<Object> line = new ArrayList<Object>();
		
		db.addTable("testTable1");
		Table table1 = db.getTable("testTable1");
		table1.addColumn("col1", DataType.INTEGER);
		table1.addColumn("col2", DataType.STRING);
		table1.addColumn("col3", DataType.BOOLEAN);
		table1.addColumn("col4", DataType.DOUBLE);
		
		line.addAll(Arrays.asList(1, "test", true, 3.5));
		table1.insert(line);
		line = new ArrayList<Object>();

		line.addAll(Arrays.asList(2, "test1q", true, 4.6));
		table1.insert(line);
		line = new ArrayList<Object>();
		
		line.addAll(Arrays.asList(3, "atest", true, 99.99));
		table1.insert(line);
		line = new ArrayList<Object>();		
		Table _table1 = table1.delete("col4=3.5");
		
		basic_test(_table1.condition("testTable1.col4=3.5").getLineCount() == 0);
	}
	
	static void UnionIntersectDB(){
		db = new Database("yey");
		db.addTable("testTable");
		Table table = db.getTable("testTable");
		table.addColumn("col1", DataType.INTEGER);
		table.addColumn("col2", DataType.STRING);
		table.addColumn("col3", DataType.BOOLEAN);
		table.addColumn("col4", DataType.DOUBLE);
		
		ArrayList<Object> line = new ArrayList<Object>();
		
		line.addAll(Arrays.asList(1, "test", false, 3.5));
		table.insert(line);
		line = new ArrayList<Object>();

		line.addAll(Arrays.asList(2, "test1q", true, 15.3));
		table.insert(line);
		line = new ArrayList<Object>();
		
		line.addAll(Arrays.asList(3, "atest", true, 99.99));
		table.insert(line);
		line = new ArrayList<Object>();
	
		db.addTable("testTable1");
		Table table1 = db.getTable("testTable1");
		table1.addColumn("col1", DataType.INTEGER);
		table1.addColumn("col2", DataType.STRING);
		table1.addColumn("col3", DataType.BOOLEAN);
		table1.addColumn("col4", DataType.DOUBLE);
		
		line.addAll(Arrays.asList(1, "test", true, 3.5));
		table1.insert(line);
		line = new ArrayList<Object>();

		line.addAll(Arrays.asList(2, "test1q", false, 4.6));
		table1.insert(line);
		line = new ArrayList<Object>();
		
		line.addAll(Arrays.asList(3, "atest", true, 99.99));
		table1.insert(line);	
	}
	
	static void TestIntersect(){
		UnionIntersectDB();
		Table table1 = db.getTable("testTable");
		Table table2 = db.getTable("testTable1");
		Table result = table1.intersect(table2);
				
		basic_test(result.getLineCount() == 1 && table1.intersect(table1).equals(table1) && 
				   table2.intersect(table2).equals(table2));
			      
	}
	
	static void TestUnion(){
		UnionIntersectDB();
		Table table1 = db.getTable("testTable");
		Table table2 = db.getTable("testTable1");
		Table result = table1.union(table2);
		
		basic_test(result.getLineCount() == table1.getLineCount() + table2.getLineCount() - table1.intersect(table2).getLineCount());
	}
	
	static void TestComplexCondition(){
		
		db = new Database("yey");
		db.addTable("testTable");
		Table table = db.getTable("testTable");
		table.addColumn("col1", DataType.INTEGER);
		table.addColumn("col2", DataType.STRING);
		table.addColumn("col3", DataType.BOOLEAN);
		table.addColumn("col4", DataType.DOUBLE);
		
		int count = 0;
		
		for(int i=0; i<100; i++){
			int c1 = i;
			String c2 = "test" + i;
			boolean c3 = new Random().nextBoolean();
			double c4 = new Random().nextDouble() * 100;
			if(c4 > 30)
				count++;
			
			table.insert(c1, c2, c3, c4);
		}
		
		basic_test(table.where("col4>30").getLineCount() == count);
		
		
	}
	
	static void TestComplexJoin() {
		
	}

	static void TestSimpleJoin() {
		db = new Database("test");
		String test1 = "test1", test2 = "test2", test3 = "test3";
		db.addTable(test1);
		db.addTable(test2);
		db.addTable(test3);
		
		Table table1 = db.getTable(test1);
		Table table2 = db.getTable(test2);
		Table table3 = db.getTable(test3);
		ArrayList<Object> line = new ArrayList<Object>();
		
		table1.addColumn("col1", DataType.INTEGER);
		table1.addColumn("col2", DataType.INTEGER);
		table1.addColumn("col3", DataType.BOOLEAN);
		line.addAll(Arrays.asList(1, 1, true));
		table1.insert(line);
		line = new ArrayList<Object>();
		
		line.addAll(Arrays.asList(1, 2, true));
		table1.insert(line);
		line = new ArrayList<Object>();
		
		line.addAll(Arrays.asList(2, 1, false));
		table1.insert(line);
		line = new ArrayList<Object>();
	
		
		table2.addColumn("col1", DataType.INTEGER);
		table2.addColumn("col2", DataType.STRING);
		table2.addColumn("col3", DataType.BOOLEAN);
		
		line.addAll(Arrays.asList(1, "gigi", true));
		table2.insert(line);
		line = new ArrayList<Object>();
		
		line.addAll(Arrays.asList(2, "dan", true));
		table2.insert(line);
		line = new ArrayList<Object>();
		
		line.addAll(Arrays.asList(3, "peter", false));
		table2.insert(line);
		line = new ArrayList<Object>();
		
		table3.addColumn("id", DataType.INTEGER);
		table3.addColumn("nume", DataType.STRING);
		table3.addColumn("este_adult", DataType.BOOLEAN);
		
		line.addAll(Arrays.asList(1, "gigi", true));
		table3.insert(line);
		line = new ArrayList<Object>();
		
		line.addAll(Arrays.asList(2, "dan", true));
		table3.insert(line);
		line = new ArrayList<Object>();
		
		line.addAll(Arrays.asList(3, "poate", false));
		table3.insert(line);

		Table joinTable = db.join(table1.getName() + ", " + table2.getName());
		Table joinTable1 = joinTable.where("test1.col1=test2.col1");
		Table joinTable2 = joinTable.where("test1.col1=test2.col1 AND test1.col2 == 1");
		
		Table joinTable3 = joinTable.where("test1.col1>test2.col1 AND test1.col2 == 1");
		Table joinTable4 = joinTable.where("test1.col1==test2.col1 AND (test2.col2==dan OR test1.col2=1)");
		
		Table joinTable5 = db.join(table1.getName() + ", " + table2.getName() + ", " + table3.getName());
		Table joinTable6 = joinTable5.where("test1.col1=test2.col1 AND test1.col2=test3.id");

		basic_test(joinTable.getLineCount() == table1.getLineCount() * table2.getLineCount() &&
				  	joinTable1.getLineCount() == 3 && joinTable2.getLineCount() == 2 &&
					joinTable3.getLineCount() == 1 && joinTable4.getLineCount() == 2 &&
					joinTable5.getLineCount() == table1.getLineCount() * table2.getLineCount() * table3.getLineCount() &&
					joinTable6.getLineCount() == 3);
	}
}
